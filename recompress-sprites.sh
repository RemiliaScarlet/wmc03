#!/bin/bash

preSize=$(du -bcd0 ./pk3-main/sprites ./pk3-main/brightmaps | tail -n1 | sed -r 's/([0-9]+).*/\1/')

for bsize in 0 64 128 192 256 512 1024 2048 4096 8192; do
    for i in `seq 0 5`; do
        find ./pk3-main/sprites/ -type f -iname '*.png' -print0 | xargs -0 -n 1 -P 22 pngout -f$i -b$bsize -kgrAb,alPh
    done
done

for bsize in 0 64 128 192 256 512 1024 2048 4096 8192; do
    for i in `seq 0 5`; do
        find ./pk3-main/brightmaps/ -type f -iname '*.png' -print0 | xargs -0 -n 1 -P 22 pngout -f$i -b$bsize -kgrAb,alPh
    done
done

postSize=$(du -bcd0 ./pk3-main/sprites ./pk3-main/brightmaps | tail -n1 | sed -r 's/([0-9]+).*/\1/')

echo "Before: $preSize bytes"
echo "After: $postSize bytes"
echo "Size difference: $((preSize - postSize)) bytes"
