module Brightmaps
  class MainProg
    def initialize(dir : Path, packName : String)
      @files = Dir["#{dir}/*.png"]

      text = String.build do |str|
        @files.each do |file|
          next if Path[file].basename.starts_with?("BM-")

          brightmappify(file)

          base = Path[file].basename
          str << "Brightmap Texture \"textures/#{packName}/#{base}\" {\n"
          str << "    map \"brightmaps/#{packName}/BM-#{base}\"\n"
          str << "}\n\n"

          File.delete(file)
        end
      end

      puts text
    end

    private def withConvert(&block : Proc(Array(String)))
      args = yield

      proc = Process.new("convert", args, error: Process::Redirect::Inherit)
      status = proc.wait

      unless status.success?
        STDERR << "Failed to run imagemagick\b"
        exit 1
      end
    end

    def brightmappify(file)
      base = Path[file].basename(".png")
      dir = Path[file].dirname

      red = "#{dir}/#{base}_000.png"
      green = "#{dir}/#{base}_001.png"
      blue = "#{dir}/#{base}_002.png"

      withConvert do
        [file,
         "(", "-clone", "0", "-channel", "R", "-separate", ")",
         "(", "-clone", "0", "-channel", "G", "-separate", ")",
         "(", "-clone", "0", "-channel", "B", "-separate", ")",
         "-delete", "0",
         "#{dir}/#{base}_%03d.png"]
      end

      withConvert { [red, "-level", "35%,100%", red] }
      withConvert { [green, "-level", "35%,100%", green] }
      withConvert { [blue, "-level", "35%,100%", blue] }

      withConvert do
        [red, green, blue,
         "-combine",
         "-blur", "0x1.09",
         "-modulate", "300,100,100",
         "#{dir}/BM-#{base}.png"]
      end

      File.delete(red)
      File.delete(green)
      File.delete(blue)
    end
  end
end

unless ARGV.size == 2
  STDERR << "Usage: make-brightmaps [directory] [pack-name]\n"
  exit 1
end

Brightmaps::MainProg.new(Path[ARGV[0]], ARGV[1])
