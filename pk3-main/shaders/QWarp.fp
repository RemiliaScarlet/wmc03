//
// Adapted from https://github.com/Novum/vkQuake/blob/master/Shaders/cs_tex_warp.comp
//

uniform float timer;

// This closly matches the turbsin lookup table (gl_warp_sin.h) in Quake
float TurboSin(float t) {
    return 8.0 * sin(t * (3.1415926535897932385 / 128.0));
}

// From gl_warp.c WARPCALC in Quake
//
// #define WARPCALC(s,t) ((s + turbsin[(int)((t*2)+(cl.time*(128.0/M_PI))) & 255]) * (1.0/64))
float WarpCalc(float s, float t) {
    float a = mod(((t * 2.0f) + (timer * (128.0 / 3.14159f))), 256.0f);
    return (s + TurboSin(a)) * (1.0 / 64.0);
}

vec4 Process(vec4 color) {
    vec2 texCoord = gl_TexCoord[0].st;
    vec2 offset   = vec2(0, 0);

    // Controls how "warpy" it is, high values shrink the texture
    float adjust = 64.0;

    float posX = texCoord.x * 1.25;
    float posY = 1.0f - (texCoord.y * 1.25);

    offset.y = WarpCalc(posX * adjust, posY * adjust);// * 0.269;
    offset.x = WarpCalc(posY * adjust, posX * adjust);// * 0.269;

    texCoord += offset;
    return getTexel(texCoord) * color;
}
