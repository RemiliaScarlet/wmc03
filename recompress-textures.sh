#!/bin/bash

recompDir="./pk3-tex/"

if [[ "$1" != "" ]]; then
    recompDir="$1"
fi

preSize=$(du -bcd0 "$recompDir" | tail -n1 | sed -r 's/([0-9]+).*/\1/')

for i in `seq 0 6`; do
    find "$recompDir" -type f -iname '*.png' -print0 | xargs -0 -n 1 -P 22 pngout -f$i -b$bsize
done

for i in `seq 0 5`; do
    find "$recompDir" -type f -iname '*.png' -print0 | xargs -0 -n 1 -P 22 pngout -f$i -b$bsize
done

postSize=$(du -bcd0 "$recompDir" | tail -n1 | sed -r 's/([0-9]+).*/\1/')

echo "Dir: $recompDir"
echo "Before: $preSize bytes"
echo "After: $postSize bytes"
echo "Size difference: $((preSize - postSize)) bytes"
