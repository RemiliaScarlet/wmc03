# WMC03

See [rules.md](rules.md) for project rules.

# Building PK3s

You will need the latest [DTools](https://osdn.net/projects/dbuildpk3/) to build
the pk3 files, as well as an ACS compiler.  Some adjustment to the pk3build.yaml
file will likely be required so that it points to the correct ACS compiler and
ACS include directory.

Once you're ready, run this to get the devkit pk3:
```
dbuildpk3 -t dev-pk3
```

Maps are not included in the devkit pk3.  For a release pk3, which includes the maps, run:
```
dbuildpk3 -t release-pk3
```

The `release-pk3` target will attempt to run advzip on the pk3 after building
it.  If you do not have advzip, it will simply print an error, but the pk3 will
still be usable.
