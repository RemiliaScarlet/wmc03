# WMC03 Biophilia

## Basics

* **Format:** UDMF
* **IWAD:** Doom 2
* **Engine:** Must be playable in GZDoom, K8Vavoom, and LZDoom.  See
  below for K8-specific details.
* **Theme:** Small, dark techbase with outdoor areas and lots of vegetation.
  Think "humid", "lots of plants", "dark and spooky, "rocks surrounded by
  trees", biological plants meets technology", "spooky greenhouses".
  [Paradise](https://www.doomworld.com/forum/topic/106058-paradise-v21-multiplayer-release-10112020/)
  by Tango is a good starting point for visual inspiration.

## Rules:

* Maps must be balanced for continuous play.  You may choose any open map slot.
* Difficulty settings must be implemented.
* Each map should be approximately 10-12 minutes long on Hurt Me Plenty.
* No more than **250** monsters per map.
* Each map must have at least one Wadazine-related secret.
* You **MUST** enable Long Texture Names when creating your map.  Ping Remilia
  on Discord if you don't understand what this means, she will help you out.
* **MAPPING DEADLINE**: August 6th, 2021.  Unused textures will be removed at
  this point.
* **PLAYTESTING**: Playtesting will commence after the mapping deadline.

## Submissions:

These must be followed, or they will NOT be accepted:

* Name your map `(username)-v(version).wad`.  For example, `remilia-v1.wad` or
  `remilia-v1.2.wad`.
* Please select a MIDI or module music file for your map, but DO NOT include it
  in the .wad file.  If you don't submit a piece of music, one will be assigned
  to you by Endless and Remilia during playtesting.
* Do **NOT** include any extra lumps in the .wad.  No music, no MAPINFO, etc.
* You can submit as many maps as you like.
* Submit a zip with your .wad, your separate music file, and a text file
  containing the map title and music credits with each update.  Also mention any
  special settings, like "no jumping" and such, in the text file.
* You can submit just your wad if it's only an update.
  
### How to Submit

All submissions and updates MUST be posted to the `#wmc-submissions` channel in
Discord.  Upload the zip directly into Discord and ping Remilia.  Failure to
ping her can easily result in your submission/update getting lost.

## Ensuring your map is playable in both GZDoom and K8Vavoom:

K8Vavoom is mostly compatible with GZDoom/LZDoom, so making a map compatible
with both isn't that difficult.  There's just a few things to keep in mind:

* No portals (skybox viewpoints are OK, however)
* No reflective floors/ceilings
* No 3D polyobjects (this is a K8 feature that is not currently supported by
  GZDoom)
* A few other minor edge cases e.g., a few rarely used ACS functions are not
  implemented

As long as you do not use these features, and follow the ACS/UDMF specs, the
maps will work in both ports.  You may have to fiddle with your lighting a bit
is all.  WMC01 MAP06, and Oops! All Greyboxes!, are examples of a maps that
works in both engines.  When in doubt, ask Remilia or look at WMC01's MAP06.
